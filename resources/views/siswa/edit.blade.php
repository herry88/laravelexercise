@extends('layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Update Siswa</h2>
            </div>
            <div class="pull-right">
                <a href="{{ route('siswa.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Data:</strong> not save <br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('siswa.update', $siswa->id) }}" method="post">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-xs-12 col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="#">Nama Siswa</label>
                        <input type="text" name="nama_siswa" class="form-control"
                         placeholder="Nama Siswa" value="{{ $siswa->nama_siswa }}">
                    </div>
                    <div class="form-group">
                        <label for="#">Alamat</label>
                        <input type="text" name="alamat" class="form-control" 
                        placeholder="Alamat" value="{{ $siswa->alamat }}">
                    </div>
                    <div class="form-group">
                        <label for="#">No Telpon</label>
                        <input type="text" name="no_telpon" class="form-control"
                         placeholder="No Telpon" value="{{ $siswa->no_telpon }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </div>
        </form>
    </div>
@endsection