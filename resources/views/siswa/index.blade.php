@extends('layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Laravel CRUD Siswa</h2>
            </div>
            <div class="pull-right">
                <a href="{{ route('siswa.create') }}" class="btn btn-success">Create Siswa</a>
            </div>
        </div>
    </div>

    @if($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>NO</th>
            <th>Nama Siswa</th>
            <th>Alamat</th>
            <th>No Telpon</th>
            <th width="280px">Action</th>
        </tr>
        @forelse ($siswa as $sw)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $sw->nama_siswa }}</td>
            <td>{{ $sw->alamat }}</td>
            <td>{{ $sw->no_telpon }}</td>
            <td>
                <form action="{{ route('siswa.destroy', $sw->id) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <a href="#" class="btn btn-info">Show</a>
                    <a href="{{ route('siswa.edit', $sw->id) }}" class="btn btn-warning">Update</a>
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>    
        @empty
            <tr>
                <td colspan="5" class="text-center">Tidak Ada Data</td>
            </tr>
        @endforelse
        
    </table>

@endsection
