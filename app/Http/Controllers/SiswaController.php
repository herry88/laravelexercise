<?php

namespace App\Http\Controllers;

use App\Siswa;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //Select * FROM siswa;
        $siswa = Siswa::all();
        return view('siswa.index', compact('siswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('siswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //untuk validasi input text
        $request->validate([
            'nama_siswa' => 'required',
            'alamat'    => 'required',
            'no_telpon' => 'required'
        ]);
        //fungsi untuk simpan ke dalam database
        // INSERT INTO siswa(nama_field) VALUES(nama_value);
        Siswa::create($request->all());
        return redirect()->route('siswa.index')->with('Siswa Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa $siswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function edit(Siswa $siswa)
    {
        //menampilkan form update 
        return view('siswa.edit', compact('siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Siswa $siswa)
    {
        //validate data input 
        $request->validate([
            'nama_siswa' => 'required',
            'alamat' => 'required',
            'no_telpon'=>'required'
        ]);
        //fungsi update database
        $siswa->update($request->all());
        return redirect()->route('siswa.index')->with('Siswa Berhasil Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Siswa $siswa)
    {
        //fungsi untuk delete field database
        $siswa->delete();
        return redirect()->route('siswa.index')->with('Siswa Berhasil Di Hapus');
    }
}
